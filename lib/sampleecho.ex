defmodule Sampleecho do
  @moduledoc """
  Documentation for Sampleecho.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Sampleecho.hello
      :world

  """
  def hello do
    :world
  end
end
